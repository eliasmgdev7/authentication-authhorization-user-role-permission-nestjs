import * as yaml from "js-yaml";
import { phases } from "./consts";
import { readFileSync } from "fs";
import { join } from "path";

const YAML_DEVELOPMENT_CONFIG = 'config.development.yaml';
const YAML_PRODUCTION_CONFIG = 'config.production.yaml';

const production_config = () => {
    return yaml.load(
        readFileSync(join(__dirname, YAML_PRODUCTION_CONFIG), 'utf8'),
    ) as Record<string, any>;
}

const development_config = () => {
    return yaml.load(
        readFileSync(join(__dirname, YAML_DEVELOPMENT_CONFIG), 'utf8'),
    ) as Record<string, any>;
}

export default () => {
    const phase = process.env.phase as unknown as phases

    if (phase === phases.PRODUCTION) return production_config()
    else return development_config()
}
