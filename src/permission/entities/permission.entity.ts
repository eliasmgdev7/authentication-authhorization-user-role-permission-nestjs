import { Field, Int, ObjectType } from "@nestjs/graphql";
import { Organization } from "src/organization/entities/organization.entity";
import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from "typeorm";

@ObjectType()
@Entity()
export class Permission {

    @PrimaryGeneratedColumn()
    @Field(type => Int)
    id: number;

    @Column()
    @Field()
    name: string;

    @OneToOne(() => Organization)
    @JoinColumn()
    @Field(type => Organization)
    organization: Organization;

    @Column("simple-array")
    @Field(type => [String])
    scopes: string[]
}