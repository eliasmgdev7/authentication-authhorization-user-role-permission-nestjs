import { Injectable } from '@nestjs/common';
import { CreateOrganizationInput } from './dto/create-organization.input';
import { UpdateOrganizationInput } from './dto/update-organization.input';
import { InjectRepository } from '@nestjs/typeorm';
import { Organization } from './entities/organization.entity';
import { Repository } from 'typeorm';

@Injectable()
export class OrganizationService {

  constructor(
    @InjectRepository(Organization)
    private repository: Repository<Organization>
  ) { }

  create(createOrganizationInput: CreateOrganizationInput): Promise<Organization | any> {
    return this.repository.save(createOrganizationInput);
  }

  findAll(): Promise<Organization[]> {
    return this.repository.find();
  }

  findOne(id: number): Promise<Organization | null> {
    return this.repository.findOneBy({ id });
  }

  update(id: number, updateOrganizationInput: UpdateOrganizationInput) {
    return `This action updates a #${id} organization`;
  }

  remove(id: number) {
    return `This action removes a #${id} organization`;
  }
}
