export enum phases {
    "DEVELOPMENT",
    "PRODUCTION"
}
export interface DatabaseConfigurationInterface {
    type: any;
    host: string;
    port: number;
    username: string;
    password: string;
    database: string;
    synchronize: boolean;
}