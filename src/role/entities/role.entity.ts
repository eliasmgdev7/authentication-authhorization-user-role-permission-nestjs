import { Field, Int, ObjectType } from "@nestjs/graphql";
import { Permission } from "src/permission/entities/permission.entity";

@ObjectType()
export class Role {

    @Field(type=>Int)
    id: number;

    @Field()
    name: string;

    @Field({nullable:true})
    descriptiuon?: string;

    @Field(type=>[Permission])
    permissions: Permission[];
}