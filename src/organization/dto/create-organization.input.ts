import { InputType, Int, Field } from '@nestjs/graphql';

@InputType()
export class CreateOrganizationInput {

  @Field()
  name: string;

  @Field(type => [String])
  scopes: string[];

  @Field({ nullable: true })
  description?: string;
}
